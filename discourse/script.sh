#!/bin/bash
service redis-server start

service postgresql start

echo "create user root superuser; alter user root with password 'postgres'; create database root;" | runuser -l postgres -c "psql" 

bundle exec rake db:create && bundle exec rake db:migrate && RAILS_ENV=test bundle exec rake db:create db:migrate


unset BUNDLE_PATH
unset BUNDLE_BIN

bundle exec rails server&
bin/ember-cli