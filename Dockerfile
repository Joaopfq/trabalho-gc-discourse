FROM ruby:bullseye

ADD . .

WORKDIR /discourse

RUN apt update

RUN apt install lsb-release -y

RUN apt-get -y install build-essential

RUN apt-get -y install libxslt1-dev libcurl4-openssl-dev libksba8 libksba-dev libreadline-dev libssl-dev zlib1g-dev libsnappy-dev

RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' &&\
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - &&\
    apt-get update  &&\
    apt-get -y install postgresql

RUN apt-get -y install libsqlite3-dev sqlite3

RUN apt install imagemagick

RUN apt-get -y install curl


RUN curl -fsSL https://packages.redis.io/gpg | gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/redis.list && \
    apt-get update && \
    apt-get -y install redis

ENV GEM_HOME="/usr/local/bundle"

ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN bundle install

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash - &&\
    apt-get -y install nodejs &&\
    npm install -g svgo &&\
    npm install -g yarn

RUN yarn install

RUN wget -qO /usr/bin/mailhog https://github.com/mailhog/MailHog/releases/download/v1.0.1/MailHog_linux_amd64 && \
    chmod +x /usr/bin/mailhog

RUN chmod +x script.sh

EXPOSE 3000/tcp 

EXPOSE 4200/tcp

CMD [ "./script.sh" ]