## Trabalho final de GC

Esse repositório foi criado para o trabalho final da disciplina de Gerência de Configuração  
  
  [Link](https://github.com/discourse/discourse) para o repositório original

## Como buildar e executar via Docker

Passos:  
  
  
1. Vá até a pasta do repositório
2. docker build . --tag discourse
3. docker run -p 3000:3000 -p 4200:4200 discourse
